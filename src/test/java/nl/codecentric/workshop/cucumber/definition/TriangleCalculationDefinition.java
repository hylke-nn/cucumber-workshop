package nl.codecentric.workshop.cucumber.definition;


import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import nl.codecentric.workshop.cucumber.TriangleCalculationApplication;
import nl.codecentric.workshop.cucumber.definition.rest.RestTriangleCalculationApplication;

import java.util.Objects;

import static org.junit.Assert.assertEquals;

/**
 * This is glue-ing Gherkin with actual code
 */
public class TriangleCalculationDefinition {

    private TriangleCalculationApplication restTriangleCalculationApplication;

    private final Triangle[] SIDES = {new Triangle(1, 2, 3), new Triangle(1, 1, 3), new Triangle(1, 1, 1)};

    final Triangle triangleDescribed = new Triangle();

    @Before
    public void before() {

        final String triangleUrl = System.getenv("TRIANGLE_URL");
        // Fail fast scenario
        Objects.requireNonNull(triangleUrl, () -> "TRIANGLE_URL variable must be set");
        restTriangleCalculationApplication = new RestTriangleCalculationApplication(triangleUrl);
    }


    @Given("Input calculator is a available")
    public void givenTriangleInputAvailable() {
        // Make client available
        restTriangleCalculationApplication.prepareForCalculation();
    }

    @When("^(\\d+) equals sides are inputted$")
    public void equalsSidesAreInputted(final int equalSides) throws Throwable {
        // Do a actual request
        if (equalSides >= 1 && equalSides <= 3) {
            Triangle sideSetup = SIDES[equalSides - 1];
            restTriangleCalculationApplication.doTriangleCalculation(sideSetup.getSide1(), sideSetup.getSide2(), sideSetup.getSide3());
        } else {
            throw new IllegalArgumentException("Number of equal sides must between 1 and 3");
        }
    }

    @Then("^the triangle is qualified as '(\\w+)'$")
    public void theTriangleIsQualifiedAsEquilateral(final String expectedTriangleType) throws Throwable {
        final String returnResult = restTriangleCalculationApplication.getCalculatedTriangleType();
        assertEquals(expectedTriangleType, returnResult);
    }

    @When("^I enter for side (\\d+) value (\\d+)$")
    public void iEnterForSideValue(int sideNumber, int sideValue) throws Throwable {
        triangleDescribed.setSideNumber(sideNumber,sideValue);
        if(triangleDescribed.hasAllSideSet()) {
            restTriangleCalculationApplication.doTriangleCalculation(triangleDescribed.getSide1(), triangleDescribed.getSide2(), triangleDescribed.getSide3());
        }
    }

    /**
     * Data structure for triangle
     */
    private static class Triangle {
        final int[] sides = new int[3];

        public Triangle() {

        }

        public Triangle(int side1, int side2, int side3) {
            this.sides[0] = side1;
            this.sides[1] = side2;
            this.sides[2] = side3;
        }

        public int getSide1() {
            return sides[0];
        }

        public int getSide2() {
            return sides[1];
        }

        public int getSide3() {
            return sides[2];
        }

        public void setSideNumber(int sideNumber, int sideValue){
            if(sideNumber >= 1 && sideNumber <= 3){
                this.sides[sideNumber-1] = sideValue;
            }
        }

        public boolean hasAllSideSet() {
            return sides[0] > 0 && sides[1] > 0 && sides[2] > 0;
        }
    }
}
